sudo apt update
sudo apt install $DOCKER_IO -y
sudo killall -9 java
sudo docker login -u "$USERNAME_DOCKERHUB" -p "$TOKEN_DOCKERHUB" $DOCKER_IO
# shellcheck disable=SC2046
sudo kill $(ps aux | grep java | grep -v 'grep' | awk '{print $2}')
sudo docker run -p 8080:8080 -d "$IMAGE_DOCKERHUB"